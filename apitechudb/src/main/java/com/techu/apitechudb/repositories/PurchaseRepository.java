package com.techu.apitechudb.repositories;

import com.techu.apitechudb.ApitechudbApplication;
import com.techu.apitechudb.excepcion.Excepciones;
import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.PurchaseModel;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@Repository
public class PurchaseRepository {

    public List<PurchaseModel> findAll() {
        System.out.println("findAll en UserRepository");
        return ApitechudbApplication.compras;
    }

    public PurchaseModel compra(PurchaseModel model){
        AtomicReference<Float> amount= new AtomicReference<>((float) 0);
        AtomicReference<Boolean> hayProducto= new AtomicReference<>(false);
        Map<String, Integer> mapa = model.getPurchaseitems();
        System.out.println("Creando Compra");
        int hayUser = ApitechudbApplication.userModels.stream().
                filter(userModel -> userModel.getId().equals(model.getUserid())).collect(Collectors.toList()).size();
        System.out.println(hayUser);

        int hayId = ApitechudbApplication.compras.stream().
                filter(purchaseModel -> purchaseModel.getId().equals(model.getId())).collect(Collectors.toList()).size();
        System.out.println(hayId);
        List<ProductModel> cProductos = new ArrayList<ProductModel>();

        ApitechudbApplication.productModels.stream().forEach(productModel -> {
            for (Map.Entry<String, Integer> clave : mapa.entrySet()){
                if(productModel.getId().equals(clave.getKey())){
                    hayProducto.set(true);
                    float precio = productModel.getPrice();
                    amount.updateAndGet(v -> new Float((float) (v + clave.getValue() * precio)));
                }
            }
        });

        System.out.println(amount.get());
        System.out.println(hayProducto);

        if (hayUser != 0 && hayId == 0 && hayProducto.get()) {
            System.out.println("entro en el if");
            ApitechudbApplication.compras.add(model);
            model.setAmount(amount.get());

       }
        return model;
    }

}
