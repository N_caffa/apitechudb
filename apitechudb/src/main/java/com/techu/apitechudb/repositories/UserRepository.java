package com.techu.apitechudb.repositories;

import com.techu.apitechudb.ApitechudbApplication;
import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.UserModel;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class UserRepository {

    public List<UserModel> findAll() {
        System.out.println("findAll en UserRepository");
        return ApitechudbApplication.userModels;
    }

    public UserModel save(UserModel model) {
        System.out.println("Save en UserRepository");
        ApitechudbApplication.userModels.add(model);
        return model;
    }

    public Optional<UserModel> findById(String id) {
        System.out.println("findById en UserRepository");
        Optional<UserModel> result = Optional.empty();

        for (UserModel userInList : ApitechudbApplication.userModels){
            if (userInList.getId().equals(id)){
                System.out.println("User encontrado en findById");
                result = Optional.of(userInList);
            }
        }
        return result;
    }

    public List<UserModel> findByAge(int age) {

        System.out.println("entro en edad");
        System.out.println("findByAge en UserRepository");

        return ApitechudbApplication.userModels.stream().
                filter(userModel -> userModel.getAge() == age).collect(Collectors.toList());


       /* List<UserModel> result = new ArrayList<UserModel>();

        for (UserModel userInList : ApitechudbApplication.userModels){
            System.out.println(userInList.getAge());
            if (userInList.getAge() == age){
                System.out.println("User encontrado en findByAge");
                result.add(userInList);
            }
        }
        return result;*/
    }

    public UserModel update (UserModel user) {
        System.out.println("update en UserRepository");

        Optional<UserModel> userToUpdate = this.findById(user.getId());

        System.out.println(userToUpdate.isPresent());

        if (userToUpdate.isPresent() == true){
            System.out.println("usuario encontrado en if de UPDATE");
            UserModel userFromList = userToUpdate.get();
            System.out.println("la edad FromList es " + userFromList.getAge());
            System.out.println("la edad ToUpdate es " + user.getAge());
            userFromList.setAge(user.getAge());
            userFromList.setName(user.getName());
        }
        return user;
    }

    public void delete(UserModel user) {
        System.out.println("delete en UserRepository");
        System.out.println("borrando usuario");
        ApitechudbApplication.userModels.remove(user);
    }
}
