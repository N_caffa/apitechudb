package com.techu.apitechudb.service;

import com.techu.apitechudb.excepcion.Excepciones;
import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.PurchaseRepository;
import com.techu.apitechudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PurchaseService {

    @Autowired
    PurchaseRepository purchaseRepository;

    public List<PurchaseModel> findAll() {
        System.out.println("findAll en UserService");

        return this.purchaseRepository.findAll();
    }

    public PurchaseModel add(PurchaseModel compra)  {
        System.out.println("add en UserService");
        return this.purchaseRepository.compra(compra);
    }
}
