package com.techu.apitechudb.service;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.ProductRepository;
import com.techu.apitechudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public List<UserModel> findAll() {
        System.out.println("findAll en UserService");

        return this.userRepository.findAll();
    }

    public UserModel add(UserModel product) {
        System.out.println("add en UserService");
        return this.userRepository.save(product);
    }

    public Optional<UserModel> findById(String id) {
        System.out.println("findById en UserService");
        return this.userRepository.findById(id);
    }

    public List<UserModel> findByAge(int age){
        System.out.println("findByAge en UserService");
        return this.userRepository.findByAge(age);
    }

    public UserModel update (UserModel userModel) {
        System.out.println("update en UserService");
        return this.userRepository.update(userModel);
    }

    public boolean delete (String id){
        System.out.println("delete en UserService");
        boolean result = false;
        Optional<UserModel> userModelToDelete = this.findById(id);
        if (userModelToDelete.isPresent() == true) {
            result = true;
            this.userRepository.delete(userModelToDelete.get());
        }
        return result;
    }
}
