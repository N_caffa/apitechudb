package com.techu.apitechudb.controllers;


import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v3")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers(@RequestParam(name = "age",defaultValue = "0",required = false) int age) {
        System.out.println("getUsers");
        if (age == 0){
            return new ResponseEntity<>(
                    this.userService.findAll(),
                    HttpStatus.OK
            );
        }else{
            System.out.println("getUserByAge");
            System.out.println("La edad del usuario a buscar es "+age);

            List<UserModel> result = this.userService.findByAge(age);

            return new ResponseEntity<>(
                    result,
                    result.isEmpty() ? HttpStatus.NO_CONTENT : HttpStatus.OK
            );

        }



    }

    @PostMapping("/users")
    public ResponseEntity<UserModel> addProduct(@RequestBody UserModel user) {
        System.out.println("addProduct");
        System.out.println("La id del usuario a crear es " + user.getId());
        System.out.println("El nombre del usuario a crear es " + user.getName());
        System.out.println("La edad del usuario a crear es " + user.getAge());

        return new ResponseEntity<>(
                this.userService.add(user),
                HttpStatus.CREATED
        );
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable String id){
        System.out.println("getUserById");
        System.out.println("La id del usuario a buscar es "+id);

        Optional<UserModel> result = this.userService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Usuario no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );

    }

  /*  @GetMapping("/users/edad")
    public ResponseEntity<List<UserModel>> getUserByAge(@RequestParam(value = "age",defaultValue = "0") int age){
        System.out.println("getUserByAge");
        System.out.println("La edad del usuario a buscar es "+age);

        List<UserModel> result = this.userService.findByAge(age);

        return new ResponseEntity<>(
                result,
                result.isEmpty() ? HttpStatus.NO_CONTENT : HttpStatus.OK
        );



    }*/


    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateProduct(@RequestBody UserModel user, @PathVariable String id) {
        System.out.println("updateProduct");
        System.out.println("La id del usuario a actualizar en parametro es " + id);
        //System.out.println("La id a actualizar es " + user.getId());
        System.out.println("El nombre del usuario a actualizar es " + user.getName());
        System.out.println("La edad del usuario a actualizar es " + user.getAge());

        return new ResponseEntity<>(this.userService.update(user), HttpStatus.OK);
    }


    @DeleteMapping("users/{id}")
    public ResponseEntity<String> deleteProduct(@PathVariable String id) {
        System.out.println("deleteProduct");
        System.out.println("La id del usuario a borrar es " + id);
        boolean deleteUser = this.userService.delete(id);
        return new ResponseEntity<>(
                deleteUser ? "Usuario borrado" : "Usuario no encontrado",
                deleteUser ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }
}
