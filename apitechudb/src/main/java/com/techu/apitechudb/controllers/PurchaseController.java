package com.techu.apitechudb.controllers;

import com.techu.apitechudb.excepcion.Excepciones;
import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.service.PurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/apitechu/v4")
public class PurchaseController {

    @Autowired
    PurchaseService purchaseService;

    @GetMapping("/compras")
    public ResponseEntity<List<PurchaseModel>> getProducts() {
        System.out.println("getProducts");

        return new ResponseEntity<>(
                this.purchaseService.findAll(),
                HttpStatus.OK
        );

    }

    @PostMapping("/compra")
    public ResponseEntity<?> addCompra(@RequestBody PurchaseModel compra) {
        System.out.println("addProduct");
        System.out.println("La id de la compra a crear es " + compra.getId());
        System.out.println("El nombre del usuario a crear es " + compra.getUserid());
        System.out.println("El precio de la compra es " + compra.getAmount());

        return new ResponseEntity<>(
                this.purchaseService.add(compra),
                HttpStatus.CREATED
        );
    }

}
