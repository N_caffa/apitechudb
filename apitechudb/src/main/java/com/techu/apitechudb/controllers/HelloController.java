package com.techu.apitechudb.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController// marcar como controlador
public class HelloController {

    @RequestMapping("/")// crear ruta
    public String index(){
        return "Hola mundo desde Api Tech U";
    }

    @RequestMapping("/hello")//@RequestParam(value = "name",defaultValue = "Tech U") se pone para requerir el parametro de entrada
    public String hello(@RequestParam(value = "name",defaultValue = "Tech U") String name){
        return String.format("hola %s",name);
    }

}
